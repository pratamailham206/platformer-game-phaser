class GameScene extends Phaser.Scene {
    constructor() {
        super("GameScene");
    }

    preload() {
        this.load.image('sky', './assets/background/pabrik.png');
        this.load.image('ground', './assets/env/magic-cliffs.png');
        this.load.spritesheet('woof', './assets/character/woof.png', { frameWidth: 32, frameHeight: 32 });
        this.load.image('diamond', './assets/env/diamond.png');
    }

    create() {
    //CREATE BACKGROUND
        this.background = this.add.image(0, 0, 'sky');
        this.background.setOrigin(0);
        this.background.setScale(1.1);

        //CREATE PLATFORM (static group)
        this.platforms = this.physics.add.staticGroup();

        this.platforms.create(400, 568, 'ground').setScale(1).refreshBody();

        this.platforms.create(500, 400, 'ground');
        this.platforms.create(150, 250, 'ground');
        this.platforms.create(0, 100, 'ground');
        this.platforms.create(700, 100, 'ground');
        this.platforms.create(700, 700, 'ground').setScale(2).refreshBody();
        
        this.score = 0;

        //CREATE DIAMON (group)
        this.diamond = this.physics.add.group({
            key: 'diamond',
            repeat: 12,
            setXY: { x: 120, y: 100, stepX: 70 }
        });
        
        this.diamond.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)); 
        });

        //CREATE TEXT
        this.TextScore = this.add.text(420, 16, 'score: 0', { fontSize: '32px', fill: '#FFFFFF' });

        function getDiamond(player, diamonds)
        {   
            diamonds.disableBody(true, true);
            this.score += 10;
            this.TextScore.setText("Score :" + this.score);
        }

        //CREATE PLAYER
        this.player = this.physics.add.sprite(160, 400, 'woof');

        this.player.setBounce(0.2);
        this.player.setCollideWorldBounds(true);

        //SET PLAYER FRAME
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('woof', { start: 0, end: 1 }),
            frameRate: 10,
            repeat: -1
        });
        
        this.anims.create({
            key: 'turn',
            frames: [ { key: 'woof', frame: 2 } ],
            frameRate: 20
        });
        
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('woof', { start: 2, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        //ADD COLLIDER ON PLATFORM & START
        this.physics.add.collider(this.player, this.platforms);
        this.physics.add.collider(this.diamond, this.platforms);

        this.physics.add.overlap(this.player, this.diamond, getDiamond, null, this);

        //INITIALIZE INPUT
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    update() {
        if (this.cursors.left.isDown)
        {
            this.player.setVelocityX(-160);
        
            this.player.anims.play('left', true);
        }
        else if (this.cursors.right.isDown)
        {
            this.player.setVelocityX(160);
        
            this.player.anims.play('right', true);
        }
        else
        {
            this.player.setVelocityX(0);
        
            this.player.anims.play('turn');
        }
        
        if (this.cursors.up.isDown && this.player.body.touching.down)
        {
            this.player.setVelocityY(-270);
        }
    
        if(this.score >= 120) {
            this.winText = this.add.text(250, 300, 'You Win', { fontSize: '56px', fill: '#000' });
            this.scene.start("ScoreScene", { score: this.score });
        }
    }
}
