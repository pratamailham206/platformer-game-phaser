class CreditScene extends Phaser.Scene {
    constructor() {
        super("CreditScene");
    }

    preload() {
        this.load.image('sky', './assets/background/pabrik.png');
    }

    create() {
        this.background = this.add.image(0, 0, 'sky');
        this.background.setOrigin(0);
        this.background.setScale(1.1)
        this.add.text(380, 200, "Credit", {font: "64px Arial", fill: "white"});
        this.add.text(370, 300, "Created using Phs");
    }
}