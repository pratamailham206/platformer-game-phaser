
class TitleScene extends Phaser.Scene {
  constructor() {
      super("bootGame");
  }

  preload() {
    this.load.image('titleBack', './assets/background/parallax-mountain.png');
    this.load.image('play', './assets/ui/PLAY.png');
    this.load.image('credit', './assets/ui/credit.png');
  }

  create() {
    //background
    this.background = this.add.image(0, 0, 'titleBack');
    this.background.setOrigin(0);
    this.background.setScale(1.9);
    

    this.add.text(120,120, "COLLECT THE DIAMONDS", {font: "60px Arial", fill: "#000"});
    //Menu
    let playBtn = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2, 'play').setDepth(1).setScale(0.6);
    let creditBtn = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2 + 100, 'credit').setDepth(1).setScale(0.3);

    playBtn.setInteractive();

    playBtn.on("pointerup", () => {
      this.scene.start("GameScene");
    });

    creditBtn.setInteractive();

    creditBtn.on("pointerup", () => {
      this.scene.start("CreditScene");
    });
  }
}

var config = {
  type: Phaser.AUTO,
  width: 1024,
  height: 720,
  physics: {
      default: 'arcade',
      arcade: {
          gravity: { y: 200 }
      }
  },
  scene: [TitleScene, GameScene, ScoreScene, CreditScene]

};

var game = new Phaser.Game(config);