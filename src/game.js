var config = {
    type: Phaser.AUTO,
    width: 1024,
    height: 720,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var player;
var platforms;
var cursors;
var diamond;
var score = 0;
var TextScore;
var winText;

function preload() {
    this.load.image('sky', './assets/background_0.png');
    this.load.image('ground', './assets/ground.png');
    this.load.spritesheet('woof', './assets/dude.png', { frameWidth: 32, frameHeight: 48 });
    this.load.image('diamond', './assets/diamond.png');
}

function create() {

    //CREATE BACKGROUND
    background = this.add.image(400, 300, 'sky');
    background.setScale(4.8);

    //CREATE PLATFORM (static group)
    platforms = this.physics.add.staticGroup();

    platforms.create(400, 568, 'ground').setScale(2).refreshBody();

    platforms.create(500, 400, 'ground');
    platforms.create(150, 250, 'ground');
    platforms.create(0, 100, 'ground');
    platforms.create(700, 100, 'ground');
    platforms.create(700, 700, 'ground').setScale(3).refreshBody();

    //CREATE DIAMON (group)
    diamond = this.physics.add.group({
        key: 'diamond',
        repeat: 12,
        setXY: { x: 120, y: 100, stepX: 70 }
    });
    
    diamond.children.iterate(function (child) {
        child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)); 
    });

    //CREATE TEXT
    TextScore = this.add.text(300, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

    function getDiamond (player, diamond)
    {   
        diamond.disableBody(true, true);
        score += 10;
        TextScore.setText("Score :" + score);
    }

    //CREATE PLAYER
    player = this.physics.add.sprite(160, 400, 'woof');

    player.setBounce(0.2);
    player.setCollideWorldBounds(true);

    //SET PLAYER FRAME
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('woof', { start: 0, end: 3 }),
        frameRate: 10,
        repeat: -1
    });
    
    this.anims.create({
        key: 'turn',
        frames: [ { key: 'woof', frame: 4 } ],
        frameRate: 20
    });
    
    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('woof', { start: 5, end: 8 }),
        frameRate: 10,
        repeat: -1
    });

    //ADD COLLIDER ON PLATFORM & START
    this.physics.add.collider(player, platforms);
    this.physics.add.collider(diamond, platforms);

    this.physics.add.overlap(player, diamond, getDiamond, null, this);

    //INITIALIZE INPUT
    cursors = this.input.keyboard.createCursorKeys();
}

function update() {
    if (cursors.left.isDown)
    {
        player.setVelocityX(-160);
    
        player.anims.play('left', true);
    }
    else if (cursors.right.isDown)
    {
        player.setVelocityX(160);
    
        player.anims.play('right', true);
    }
    else
    {
        player.setVelocityX(0);
    
        player.anims.play('turn');
    }
    
    if (cursors.up.isDown && player.body.touching.down)
    {
        player.setVelocityY(-270);
    }

    if(score >= 110) {
        winText = this.add.text(250, 300, 'You Win', { fontSize: '56px', fill: '#000' });
    }
}
  