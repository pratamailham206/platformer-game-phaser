  
class ScoreScene extends Phaser.Scene{
    constructor(){
      super("ScoreScene");
    }

    init(data) {
      this.score = data.score;
    }

    preload() {
      this.load.image('scoreBackdrop', './assets/background/pabrik.png');
    }
  
    create(){
      this.background = this.add.image(0, 0, 'scoreBackdrop');
      this.background.setOrigin(0);
      this.background.setScale(1.2)
      this.add.text(380, 200, "You WIN", {font: "64px Arial", fill: "white"});
      this.add.text(370, 300, "Your Score: " + this.score, {font: "42px Arial", fill: "white"});      

      let PlayAgainBtn = this.add.text(200, 450, "<Play Again>", {font: "28px Arial", fill: "white"});
      let BackToMenu =  this.add.text(620, 450, "<Back to Menu>", {font: "28px Arial", fill: "white"});

      PlayAgainBtn.setInteractive();

      PlayAgainBtn.on("pointerup", () => {
        this.scene.start("GameScene");
      });

      BackToMenu.setInteractive();

      BackToMenu.on("pointerup", () => {
        this.scene.start("bootGame");
      });
    }
  }